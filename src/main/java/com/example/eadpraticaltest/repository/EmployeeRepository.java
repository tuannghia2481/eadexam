package com.example.eadpraticaltest.repository;

import com.example.eadpraticaltest.entity.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee,Integer> {
}
