package com.example.eadpraticaltest.controller;

import com.example.eadpraticaltest.entity.Employee;
import com.example.eadpraticaltest.repository.EmployeeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeRepository;

   @RequestMapping("/")
public String index (Model model){
        List<Employee> employees = (List<Employee>) employeeRepository.findAll();
       model.addAttribute("employees",employees);
       return "index";
    }




    @RequestMapping(value = "add")
    public String addEmployee(Model model){
        model.addAttribute("employee",new Employee());
        return "addEmployee";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public  String save(Employee employee){
        employeeRepository.save(employee);
        return "redirect:/";
    }



    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editEmployee(@RequestParam("id") Integer employeeId , Model model){
       Optional<Employee> empsEdit = employeeRepository.findById(employeeId);
       empsEdit.ifPresent(employee -> model.addAttribute("employee",employee));
        return "editEmployee";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteEmployee(@RequestParam("id") Integer employeeId, Model model){
        employeeRepository.deleteById(employeeId);
        return "redirect:/";
    }
}
