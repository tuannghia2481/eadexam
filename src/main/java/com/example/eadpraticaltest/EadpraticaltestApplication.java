package com.example.eadpraticaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EadpraticaltestApplication {

    public static void main(String[] args) {
        SpringApplication.run(EadpraticaltestApplication.class, args);
    }

}
